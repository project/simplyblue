<?php
/**
 * @file
 * Page template.
 */
?>
<!--------------Header--------------->
<header> 
  <div class="zerogrid">
    <div class="row">
      <div class="col05">
        <div id="logo">
          <?php if ($logo): ?><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
          <?php else: ?>
            <h1>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" style="color:#fff;"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>
        </div>
      </div>
      <div class="col06 offset05">
        <div id='search-box'>
          <?php if ($page['search']): ?>
            <?php print render($page['search']); ?>
          <?php endif; ?>	
        </div>
      </div>
    </div>
  </div>
</header>

<!--------------Navigation--------------->

<nav>
  <?php
  if (module_exists('i18n_menu')) {
    $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
  }
  else {
    $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
  }
  print drupal_render($main_menu_tree);
  ?>
</nav>


<!--------------Content--------------->
<section id="content">
  <div class="zerogrid">
    <div class="row block">
      <div class="main-content col11">
      <?php if ($messages): ?>
        <div id="messages"><div class="section clearfix">
          <?php print $messages; ?>
        </div></div> <!-- /.section, /#messages -->
      <?php endif; ?>
        <?php if ($is_front): ?>
          <?php if (theme_get_setting('slideshow_display', 'simplyblue')): ?>
            <?php if ($page['slideshow']): ?>
              <?php print render($page['slideshow']); ?>
            <?php endif; ?>	
          <?php endif; ?>
        <?php endif; ?>
        <?php if (theme_get_setting('breadcrumbs')): ?>
            <?php if ($breadcrumb): ?>
                <div id="breadcrumb">
                    <?php print $breadcrumb; ?>
                 </div>
             <?php endif;?>
         <?php endif; ?>
         
         <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
        
        <div <?php echo $content_col; ?>><?php print render($page['content_top']); ?></div>

        <div <?php echo $content_col; ?>><?php print render($page['content']); ?></div>

      </div>
      <?php if ($page['sidebar']): ?>
        <div class="sidebar col05">
          <?php print render($page['sidebar']); ?>
        </div>
      <?php endif; ?>	

    </div>
  </div>
</section>
<!--------------Footer--------------->
<footer>
  <div class="zerogrid">
    <div class="row">
      <?php if ($page['bottom_first']): ?>  
        <section class="col-1-3">
          <?php print render($page['bottom_first']); ?>
        </section>
      <?php endif; ?>
      
      <?php if ($page['bottom_second']): ?>
        <section class="col-1-3">
          <?php print render($page['bottom_second']); ?>
        </section>
      <?php endif; ?>
      
      <?php if ($page['bottom_third']): ?>
        <section class="col-1-3">
          <?php print render($page['bottom_third']); ?>
        </section>
      <?php endif; ?>
    </div>
  </div>
</footer>
<?php if ($page['footer']): ?>
  <div id="copyright">
    <?php print render($page['footer']); ?>        
  </div>
<?php endif; ?>
