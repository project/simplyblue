<?php

/**
 * @file
 * Common template.
 */

/**
 * This will overwrite the default meta character type tag with HTML5 version.
 */
function simplyblue_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8',
  );
}

/**
 * Insert themed breadcrumb page navigation at top of the node content.
 */
function simplyblue_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Use CSS to hide titile .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    // Comment below line to hide current page to breadcrumb.
    $breadcrumb[] = drupal_get_title();
    $output .= '<div>' . implode(' » ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override or insert variables into the page template.
 */
function simplyblue_preprocess_page(&$variables) {
  if (empty($variables['page']['sidebar'])) {
    $variables['content_col'] = ' class="col16"';
  }
  else {
    $variables['content_col'] = ' class="col8"';
  }
  if (isset($variables['main_menu'])) {
    $variables['main_menu'] = theme('links__system_main_menu', array(
      'links' => $variables['main_menu'],
      'attributes' => array(
        'class' => array('links', 'main-menu', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  else {
    $variables['main_menu'] = FALSE;
  }
  if (isset($variables['secondary_menu'])) {
    $variables['secondary_menu'] = theme('links__system_secondary_menu', array(
      'links' => $variables['secondary_menu'],
      'attributes' => array(
        'class' => array('links', 'secondary-menu', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  else {
    $variables['secondary_menu'] = FALSE;
  }
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function simplyblue_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Override or insert variables into the node template.
 */
function simplyblue_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  $variables['date'] = t('!datetime', array('!datetime' => date('j F Y', $variables['created'])));
}

/**
 * To overrides the page header.
 */
function simplyblue_page_alter($page) {
  $viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1',
    ),
  );
  drupal_add_html_head($viewport, 'viewport');
}

/**
 * Added dynamic class to blocks.
 */
function simplyblue_preprocess_block(&$variables) {
  $region = $variables['elements']['#block']->region;
  $blocks = array(
    'search',
    'bottom_first',
    'bottom_second',
    'bottom_third',
    'footer',
  );
  if (in_array($region, $blocks)) {
    $variables['classes_array'][] = 'zerogrid_block_alter';
  }
}

/**
 * Remove the default system css.
 */
function simplyblue_css_alter(&$css) {
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
}
