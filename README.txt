Simply Blue is simple, elegant and responsive Drupal theme. 
It can be used for blogs, small business, movies 
and variety of other websites. 
The theme was designed using flat elements for 
the header, navigation, buttons and more. 
You can easily modify this theme as per 
your requirement. It is very flexible 
and easy to re-design as well.

Requirements:
This theme is compatible with Drupal 7.x

Installation:
Uzip the folder and upload the theme folder 
to sites/all/themes/ directory.
Go to admin/appearance 
and set default theme as Simply Blue
Click the Save Configuration and you are done.

Theme by:
http://www.zerotheme.com/
